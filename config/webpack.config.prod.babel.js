/* global __dirname */
import webpack from 'webpack'
import config from './webpack.config'

export default config({
  devtool: 'source-map',

  plugins: [
    new webpack.LoaderOptionsPlugin({
      minimize: true,
      debug: false,
    }),
  ],
})
