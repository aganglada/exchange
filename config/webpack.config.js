/* global __dirname */
import webpack from 'webpack'
import path from 'path'
import packageJson from '../package.json'
import HtmlPlugin from 'html-webpack-plugin'

const basePath = path.join(__dirname, '..', 'src')
const env = process.env.NODE_ENV || 'development'

console.log('Webpack running in ' + env)
console.log('App running at http://localhost:4000')

export default ({ plugins = [], devtool = 'eval' }) => {
  return {
    entry: {
      app: path.join(basePath, 'index.js'),
      vendor: Object.keys(packageJson.dependencies),
    },

    output: {
      path: path.join(basePath, '..', 'assets'),
      publicPath: env === 'development' ? '/' : '',
      filename: '[name].js',
    },

    devtool,

    plugins: [
      new HtmlPlugin({
        title: 'Exchange rate',
        template: path.join(basePath, 'index.html'),
      }),
    ].concat(plugins),

    optimization: {
      splitChunks: {
        name: 'vendor',
        filename: '[name].js',
      },
    },

    module: {
      rules: [
        {
          test: /\.jsx?$/,
          loader: 'babel-loader',
          include: [basePath],
        },
      ],
    },

    devServer: {
      noInfo: true,
      port: 4000,
      contentBase: path.join(basePath, 'assets'),
    },
  }
}
