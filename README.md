# Exchange

### Tech stack

* React
* Redux
* Reselect
* Unfetch
* Styled Components
* Webpack
* Jest

### Running it locally

1. Clone the repo.
2. Run `yarn` or `npm i` to install dependencies.
3. Run `yarn dev` to run de dev server.

### Running the tests

Run `yarn test` in your terminal.
