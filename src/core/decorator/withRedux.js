import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

const mergeDispatchToProps = actions => {
  // Null/undefined skips the action binding
  if (actions == null) {
    return null
  }

  if (typeof actions !== 'object') {
    throw new Error('connectDispatch needs to be an object')
  }

  const actionCreatorKeys = Object.keys(actions)

  return dispatch =>
    actionCreatorKeys.reduce((boundCreators, newKey) => {
      boundCreators[newKey] = bindActionCreators(actions[newKey], dispatch)
      return boundCreators
    }, {})
}

export default options => {
  if (!options) return Component => Component

  const { connectState = null, connectDispatch = null } = options

  return Component => {
    let Wrapper = Component

    if (connectState || connectDispatch) {
      Wrapper = connect(
        connectState,
        mergeDispatchToProps(connectDispatch),
      )(Wrapper)
    }

    return Wrapper
  }
}
