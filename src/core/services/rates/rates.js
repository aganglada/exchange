import fetch from 'unfetch'
import qs from 'qs'

const BASE_URL = 'https://api.exchangeratesapi.io/latest'

export default {
  refresh: ({ base, to }) => {
    const url = `${BASE_URL}${qs.stringify(
      {
        base,
        symbols: to,
      },
      { addQueryPrefix: true },
    )}`

    return fetch(url).then(res => res.json())
  },
}
