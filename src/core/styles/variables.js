export const color = {
  grey: '#ebeff3',
  fuxia: '#EB008D',
  blue: '#0d6fcd'
}

export const size = {
  spacer: '10px',
  header: '60px'
}