import { injectGlobal } from 'styled-components'

injectGlobal`

  *.
  *:before,
  *:after {
    box-sizing: border-box;
  }

  html {
    line-height: 1.15; 
    -ms-text-size-adjust: 100%; 
    -webkit-text-size-adjust: 100%; 
  }

  body {
    margin: 0;
    font-family: Helvetica Neue,Helvetica,Arial,sans-serif;
    font-style: normal;
    font-weight: 300;
  }

  html,
  body,
  .full,
  [data-reactroot] {
    height: 100%;
    width: 100%;
  }

  a {
    background-color: transparent; 
    -webkit-text-decoration-skip: objects; 
  }

  [hidden] {
    display: none;
  }

  ul, ol {
    list-style: none;
    padding: 0
  }
`
