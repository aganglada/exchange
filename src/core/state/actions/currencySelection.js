import action from '../utils/action'

export const CURRENCY_SELECTION__UPDATE_AMOUNT =
  'CURRENCY_SELECTION__UPDATE_AMOUNT'
export const CURRENCY_SELECTION__UPDATE_CODE = 'CURRENCY_SELECTION__UPDATE_CODE'
export const CURRENCY_SELECTION__TOGGLE = 'CURRENCY_SELECTION__TOGGLE'

const getOpossiteSelectionId = ({ currencySelection }, id) => {
  return Object.keys(currencySelection).filter(key => key !== id)[0]
}

export default {
  updateCodeSelection: (id, code) => (dispatch, getState) => {
    const state = getState()
    const oppositeSelectionId = getOpossiteSelectionId(state, id)

    if (code === state.currencySelection[oppositeSelectionId].code) {
      // only if both codes are equal
      dispatch(
        action(CURRENCY_SELECTION__UPDATE_CODE, {
          id: oppositeSelectionId,
          code: state.currencySelection[id].code,
        }),
      )
    }

    dispatch(action(CURRENCY_SELECTION__UPDATE_CODE, { id, code }))
  },
  updateAmountSelection: (id, amount) => (dispatch, getState) => {
    const state = getState()
    const oppositeId = getOpossiteSelectionId(state, id)

    dispatch(action(CURRENCY_SELECTION__UPDATE_AMOUNT, { id, amount }))
    dispatch(
      action(CURRENCY_SELECTION__UPDATE_AMOUNT, {
        id: oppositeId,
        amount: '',
      }),
    )
  },
  toggleSelection: () => action(CURRENCY_SELECTION__TOGGLE),
}
