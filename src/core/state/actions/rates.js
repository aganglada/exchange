import action from '../utils/action'

export const RATES__UPDATE = 'RATES__UPDATE'

export default {
  updateRates: rates => action(RATES__UPDATE, rates),
}
