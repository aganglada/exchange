import action from '../action'

let result
const type = 'TYPE__TEST'
const payload = { example: 'hello' }
const reaction = () => {}

beforeEach(() => {
  result = action(type, payload, reaction)
})

describe('State -> Utils -> action', () => {
  it('should return an object formed by the three arguments passed', () => {
    expect(result).toEqual({
      type,
      payload,
      reaction,
    })
  })

  it('should expect type to be string', () => {
    expect(typeof result.type).toBe('string')
  })

  it('should expect payload to be object if object is passed', () => {
    expect(typeof result.payload).toBe('object')
  })

  it('should expect reaction to be function', () => {
    expect(typeof result.reaction).toBe('function')
  })
})
