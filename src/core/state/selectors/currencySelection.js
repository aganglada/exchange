import { createSelector } from 'reselect'

const currencies = state => state.currencies
const rates = state => state.rates
const selectorId = (state, props) => props.id
const currencySelection = state => state.currencySelection

const getCurrencyDataFromCode = (code, currencies) => {
  return currencies.filter(currency => currency.code === code)[0]
}

const getCurrentCodeFromSelection = (selectorId, currencySelection) => {
  return currencySelection[selectorId].code
}

const parseSelections = (currencySelection, rates) => {
  const base = currencySelection.base.amount
  const to = currencySelection.to.amount

  // if both are empty, no changes, return currencySelection
  if (base === '' && to === '') return currencySelection

  const selectionKeys = Object.keys(currencySelection)
  // find out what's the not empty amount key so we can use
  // it later to parse the value
  const notEmptyKey = selectionKeys.filter(
    key => currencySelection[key].amount !== '',
  )[0]

  return selectionKeys.reduce((list, selectionKey) => {
    const selection = currencySelection[selectionKey]
    // check if the current key is empty
    // so we can give it a value calculating
    // with non empty * current "to" rate
    const amount =
      selectionKey !== notEmptyKey
        ? new Number(
            currencySelection[notEmptyKey].amount *
              rates[currencySelection.to.code],
          ).toFixed(2)
        : selection.amount

    return {
      ...list,
      [selectionKey]: {
        ...currencySelection[selectionKey],
        amount,
      },
    }
  }, {})
}

export const selectionsSelector = createSelector(
  currencySelection,
  rates,
  parseSelections,
)

export const getCodeFromSelectionSelector = createSelector(
  selectorId,
  currencySelection,
  getCurrentCodeFromSelection,
)

export const getCurrencyDataFromCodeSelector = createSelector(
  getCodeFromSelectionSelector,
  currencies,
  getCurrencyDataFromCode,
)
