import { createSelector } from 'reselect'

const rates = state => state.rates

const parseRates = rates => {
  return Object.keys(rates).reduce((list, rate) => {
    return {
      ...list,
      [rate]: rates[rate] ? rates[rate].toFixed(4) : 0,
    }
  }, {})
}

export const ratesSelector = createSelector(rates, parseRates)
