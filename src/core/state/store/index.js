import { createStore, applyMiddleware, combineReducers } from 'redux'
import thunkMiddleware from 'redux-thunk'

import currencies from '../fixtures/currency.symbols.json'

import rates from '../reducers/rates'
import currencySelection from '../reducers/currencySelection'
import dataOnly from '../reducers/dataOnly'

const reducer = combineReducers({
  rates,
  currencySelection,
  currencies: dataOnly,
})

const intialState = {
  currencies,
}

export default createStore(
  reducer,
  intialState,
  applyMiddleware(thunkMiddleware),
)
