import action from '../../utils/action'
import reducer from '../dataOnly'

const reducerName = 'DATA_ONLY'

const state = {
  loading: false,
  position: {},
  permitted: true,
}

describe(`reducer: ${reducerName}`, () => {
  it('should default the initial state', () => {
    const _action = action(reducerName)

    expect(reducer(state, _action)).toEqual(state)
  })

  it('should be a pure function', () => {
    const _action = action(reducerName)

    expect(reducer(undefined, _action)).toEqual({})
  })
})
