import action from '../../utils/action'
import reducer from '../rates'
import {
  CURRENCY_SELECTION__UPDATE_CODE,
  CURRENCY_SELECTION__UPDATE_AMOUNT,
  CURRENCY_SELECTION__TOGGLE,
} from '../../actions/currencySelection'

const reducerName = 'CURRENCY_SELECTION'

let state

describe(`reducer: ${reducerName}`, () => {
  beforeEach(() => {
    state = reducer(undefined, action('TEST'))
  })

  it('should default the initial state', () => {
    const _action = action(reducerName)

    expect(reducer(state, _action)).toEqual(state)
  })

  it('should be a pure function', () => {
    const _action = action(reducerName)

    expect(reducer(undefined, _action)).toEqual({})
  })

  describe(`action type: ${CURRENCY_SELECTION__UPDATE_CODE}`, () => {
    it('should update the code to the respective id passed', () => {
      const _payload = { id: 'base', code: 'GBP' }
      const _action = action(CURRENCY_SELECTION__UPDATE_CODE, _payload)
      const _result = reducer(state, _action)
      

      expect(_result).toEqual()
    })
  })
})
