import action from '../../utils/action'
import reducer from '../rates'
import { RATES__UPDATE } from '../../actions/rates'

const reducerName = 'RATES'

const state = {}
const mockRates = {
  GBP: '0.800',
  EUR: '1.111',
}

describe(`reducer: ${reducerName}`, () => {
  it('should default the initial state', () => {
    const _action = action(reducerName)

    expect(reducer(state, _action)).toEqual(state)
  })

  it('should be a pure function', () => {
    const _action = action(reducerName)

    expect(reducer(undefined, _action)).toEqual({})
  })

  describe(`action type: ${RATES__UPDATE}`, () => {
    it('should update the state as rates after it is executed', () => {
      const _action = action(RATES__UPDATE, mockRates)

      expect(reducer(state, _action)).toEqual(mockRates)
    })
  })
})
