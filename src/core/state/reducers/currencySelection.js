import {
  CURRENCY_SELECTION__UPDATE_AMOUNT,
  CURRENCY_SELECTION__UPDATE_CODE,
  CURRENCY_SELECTION__TOGGLE,
} from '../actions/currencySelection'

const initialState = {
  base: { code: 'EUR', amount: '' },
  to: { code: 'GBP', amount: '' },
}

export default (state = initialState, action) => {
  switch (action.type) {
    case CURRENCY_SELECTION__UPDATE_CODE:
      return {
        ...state,
        [action.payload.id]: {
          ...state[action.payload.id],
          code: action.payload.code,
        },
      }

    case CURRENCY_SELECTION__UPDATE_AMOUNT:
      return {
        ...state,
        [action.payload.id]: {
          ...state[action.payload.id],
          amount: action.payload.amount,
        },
      }

    case CURRENCY_SELECTION__TOGGLE:
      return {
        base: state.to,
        to: state.base,
      }

    default:
      return state
  }
}
