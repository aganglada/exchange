import { RATES__UPDATE } from '../actions/rates'

export default (state = {}, action) => {
  switch (action.type) {
    case RATES__UPDATE:
      return {
        ...state,
        ...action.payload,
      }

    default:
      return state
  }
}
