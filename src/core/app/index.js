import React from 'react'
import { Provider } from 'react-redux'
import store from '../state/store'

import Header from '../../components/header/header'
import Exchange from '../../views/exchange/exchange'

class App extends React.PureComponent {
  render() {
    return (
      <Provider store={store}>
        <React.Fragment>
          <Header title="Exchange" />
          <Exchange />
        </React.Fragment>
      </Provider>
    )
  }
}

export default App
