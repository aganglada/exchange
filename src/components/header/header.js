import React from 'react'

import { Container, Title } from './header.styled'

class Header extends React.PureComponent {
  render() {
    return (
      <Container>
        <Title>{this.props.title}</Title>
      </Container>
    )
  }
}

export default Header
