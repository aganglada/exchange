import styled from 'styled-components'

import { size, color } from '../../core/styles/variables'

export const Container = styled.header`
  height: ${size.header};
`

export const Title = styled.h1`
  margin: 0;

  line-height: ${size.header};
  text-align: center;
  font-weight: 300;
  color: ${color.fuxia};
`
