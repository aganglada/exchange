import React from 'react'
import PropTypes from 'prop-types'

import { Container, Selector, Balance } from './currencySelector.styled'

class CurrencySelector extends React.PureComponent {
  static propTypes = {
    currencies: PropTypes.array.isRequired,
    currency: PropTypes.object.isRequired,
    onChange: PropTypes.func.isRequired,
  }

  _onSelectorChange = ({ target }) => {
    this.props.onChange(this.props.id, target.value)
  }

  render() {
    const { currencies, currency } = this.props

    return (
      <Container>
        <Selector value={currency.code} onChange={this._onSelectorChange}>
          {currencies.map(item => {
            return (
              <option key={item.code} value={item.code}>
                {item.code}
              </option>
            )
          })}
        </Selector>
        <Balance>
          Balance: {currency.symbol}
          {currency.balance}
        </Balance>
      </Container>
    )
  }
}

export default CurrencySelector
