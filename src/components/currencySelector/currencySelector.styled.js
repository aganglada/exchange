import styled from 'styled-components'

import { color, size } from '../../core/styles/variables'

const local = {
  arrow: `data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" width="100" height="100" fill="#000"><polygon points="0,0 100,0 50,50"/></svg>`,
}

export const Container = styled.div`
  width: 70%;
  margin: 0 auto;
`

export const Selector = styled.select`
  border: 0;
  padding-right: calc(${size.spacer} * 2);

  appearance: none;
  background-color: transparent;
  background-image: url('${local.arrow}');
  background-size: 12px;
  background-position: center right;
  background-repeat: no-repeat;
  font-size: 2em;
`

export const Balance = styled.p`
  padding: calc(${size.spacer} / 2);
  margin: 0;
`
