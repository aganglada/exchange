import styled from 'styled-components'

export const InputNumber = styled.input`
  width: 70%;
  margin: 0 auto;
  border: 0;

  appearance: none;
  background: transparent;
  font-size: 3em;
  text-align: right;
`
