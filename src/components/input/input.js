import React from 'react'

import { InputNumber } from './input.styled'

class Input extends React.PureComponent {
  _onInputChange = ({ target }) => {
    this.props.onChange(this.props.id, target.value)
  }

  render() {
    const { amount } = this.props

    return (
      <InputNumber
        value={amount}
        onChange={this._onInputChange}
        type="number"
        placeholder="0"
      />
    )
  }
}

export default Input
