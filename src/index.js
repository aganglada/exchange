import React from 'react'
import ReactDOM from 'react-dom'
import App from './core/app'

import './core/styles/base'

ReactDOM.render(<App />, document.getElementById('💰'))
