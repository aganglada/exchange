import styled, { css } from 'styled-components'

import { color, size } from '../../core/styles/variables'

import IncreaseIcon from '../../resources/svgs/increase.svg'
import SwapIcon from '../../resources/svgs/swap.svg'

const local = {
  ratewidth: '200px',
}

export const Grid = styled.main`
  height: calc(100% - ${size.header});

  display: grid;
  justify-content: center;
  align-content: center;
  grid-template-columns: repeat(2, 1fr);
  grid-template-rows: repeat(2, 50%);

  position: relative;
`

export const Box = styled.article`
  display: flex;
  align-items: center;
  justify-content: center;

  background-color: ${props => color[props.color]};
`

const baseCentered = css`
  position: absolute;
  top: 50%;
  display: flex;
  align-items: center;
  justify-content: center;
  border: 2px solid ${color.grey};
  height: calc(${size.spacer} * 3);

  background-color: white;
  border-radius: calc(${size.spacer} * 1.5);
  line-height: calc(${size.spacer} * 3);
  color: ${color.blue};
`

export const Rate = styled.div`
  ${baseCentered};
  width: ${local.ratewidth};
  margin-left: calc(${local.ratewidth} / -2);
  margin-top: calc(${size.spacer} * -1.5);
  left: 50%;
`

export const Increase = styled(IncreaseIcon)`
  height: 25px;
  margin-right: 10px;
`

export const SwapButton = styled.button`
  ${baseCentered};
  width: calc(${size.spacer} * 3.5);
  height: calc(${size.spacer} * 3.5);
  margin-top: -15px;
  border-radius: 50%;

  left: 5%;
`

export const Swap = styled(SwapIcon)`
  height: 20px;
`
