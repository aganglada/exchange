import React from 'react'
import withRedux from '../../core/decorator/withRedux'

import Input from '../../components/input/input'
import CurrencySelector from '../../components/currencySelector/currencySelector'

import rates from '../../core/services/rates/rates'
import ratesActions from '../../core/state/actions/rates'
import currencySelectionActions from '../../core/state/actions/currencySelection'
import {
  getCurrencyDataFromCodeSelector,
  selectionsSelector,
} from '../../core/state/selectors/currencySelection'
import { ratesSelector } from '../../core/state/selectors/rates'

import { Grid, Box, Rate, Increase, SwapButton, Swap } from './exchange.styled'

class Exchange extends React.PureComponent {
  interval = null

  componentWillMount() {
    this._updateRates()
    this.interval = setInterval(() => {
      this._updateRates()
    }, 10000)
  }

  componentWillUnmount() {
    clearInterval(this.interval)
  }

  componentDidUpdate(nextProps) {
    const { base, to } = this.props.selections
    if (
      base.code !== nextProps.selections.base.code ||
      to.code !== nextProps.selections.to.code
    ) {
      this._updateRates()
    }
  }

  _updateRates = () => {
    const { base, to } = this.props.selections

    rates.refresh({ base: base.code, to: to.code }).then(response => {
      this.props.updateRates(response.rates)
    })
  }

  render() {
    const {
      baseData,
      toData,
      rates,
      currencies,
      selections,
      updateCodeSelection,
      updateAmountSelection,
      toggleSelection,
    } = this.props

    return (
      <Grid>
        <Box>
          <CurrencySelector
            currencies={currencies}
            currency={baseData}
            onChange={updateCodeSelection}
            id={'base'}
          />
        </Box>
        <Box>
          <Input
            onChange={updateAmountSelection}
            id={'base'}
            amount={selections.base.amount}
          />
        </Box>
        <SwapButton onClick={toggleSelection}>
          <Swap />
        </SwapButton>
        <Rate>
          <Increase />
          {baseData.symbol}1 = {toData.symbol}
          {rates[toData.code]}
        </Rate>
        <Box color="grey">
          <CurrencySelector
            currencies={currencies}
            currency={toData}
            onChange={updateCodeSelection}
            id={'to'}
          />
        </Box>
        <Box color="grey">
          <Input
            onChange={updateAmountSelection}
            id={'to'}
            amount={selections.to.amount}
          />
        </Box>
      </Grid>
    )
  }
}

export default withRedux({
  connectState: state => {
    return {
      currencies: state.currencies,
      selections: selectionsSelector(state),
      rates: ratesSelector(state),
      baseData: getCurrencyDataFromCodeSelector(state, { id: 'base' }),
      toData: getCurrencyDataFromCodeSelector(state, { id: 'to' }),
    }
  },
  connectDispatch: {
    updateRates: rates => ratesActions.updateRates(rates),
    updateCodeSelection: currencySelectionActions.updateCodeSelection,
    updateAmountSelection: currencySelectionActions.updateAmountSelection,
    toggleSelection: currencySelectionActions.toggleSelection,
  },
})(Exchange)
